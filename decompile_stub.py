#!/usr/bin/env python

import os, sys, argparse
from androguard.core.bytecodes import apk
from androguard.core.bytecodes import dvm
from androguard.core.analysis import analysis
from androguard.decompiler.dad import decompile
from termcolor import colored
import xml

def getClassPath(className):
    # va returna calea unde va trebui salvata o anumita clasa
    # get_name() pentru un obiect ClassDefItem va returna un nume de clasa care
    # va incepe cu "L" si se termina cu ";" ex: Ljava/lang/Object;
	return '%s.java' % className[1:-1]

def getClassName(className):
    # va returna numele unei clase
    # ex. pentru Ljava/lang/Object; va returna: java.lang.Object
	return className[1:-1].replace('/', '.')

def makeDirs(directory):
    # va crea un director cu toate subdirectoarele care inca nu exista
    if not os.path.isdir(directory):
        os.makedirs(directory)

def decompileMethod(methodObj, analysis):
    # <methodObj> e un obiect de tipul EncodedMethod
    # <analysis> e un obiect de tipul VMAnalysis
    if methodObj.get_code() == None:
        return None
    methodAnalysis = analysis.get_method(methodObj)    # returns MethodAnalysis object
    decompMethod = decompile.DvMethod(methodAnalysis)
    try:
        decompMethod.process()
        methodSource = decompMethod.get_source()
    except:
        print 'Failed to decompile [%s]' % methodObj.get_name()
        return '''   method %s() {
        // failed to decompile
    }'''
    return methodSource

print("\nBegin Parsing:...\n")

parser = argparse.ArgumentParser(description='Decompiler for APKs')

parser.add_argument('apkpath', help='path to apk')
parser.add_argument('-manifest', action='store_true', help='save AndroidManifest.xml')
parser.add_argument('-perms', action='store_true', help='list permissions')
parser.add_argument('-activities', action='store_true', help='list activities')
parser.add_argument('-decomp', action='store_true', help='decompile apk')

args = parser.parse_args()

apkpath = args.apkpath
apkobj = apk.APK(apkpath)

if args.manifest:
	print colored("MANIFEST FOUND!", 'green')
	manifest = apkobj.get_AndroidManifest()
	print colored("\nSaving Manifest to AndroidManifest.xml...", 'green')
	f= open('AndroidManifest.xml', 'wb')
	manifest.writexml(f, encoding= 'utf-8')
	f.close()
    # save manifest

if args.perms:
	print("\nChecking Permissions:...")
	permissions = apkobj.get_permissions()
	if not permissions:
		print colored("NO PERMISSIONS", 'red')
	else:
		print colored("=> Permissions:", 'green')
		for aPer in permissions:
			print("	"+aPer)
    # list permissions

if args.activities:
	print("\nChecking Activities:...")
	activities = apkobj.get_activities()
	if not activities:
		print colored("NO ACTIVITIES!", 'red')
	else:
		print colored("=> Activities:", 'green')
		for anAct in activities:
			print("	"+anAct)
    # list activities


if args.decomp:
    print colored("\nDecompiling...", 'green')
    rawdex = apkobj.get_dex()
    dex = dvm.DalvikVMFormat(rawdex, decompiler='dad')
    analysis = analysis.VMAnalysis(dex)

    allClasses = dex.get_classes()
    if not allClasses:
    	print colored("NO CLASSES!", 'red')
    else:
	print("=>Classes:")
	for idx, aClass in enumerate (allClasses):
		
		#aClass.source()		
		#print aClass.__class__
		print("\n"+11*"~=~=~=")
		print colored("Class number#"+str(idx)+" [name:] "+getClassName(aClass.get_name()), 'green')

		superClass = getClassName(aClass.get_superclassname())
		if superClass:
			print colored("Super Class: "+superClass, 'yellow')

		itsInterfaces = aClass.get_interfaces()
		if itsInterfaces:		
			print colored("Interfaces:", 'yellow'),
			for anInterface in itsInterfaces:
				print colored(anInterface, 'yellow')

		itsFields = aClass.get_fields()
		if itsFields: 
			print colored("Fields:", 'yellow'), 
			for aField in itsFields:
				print colored(" "+aField.get_name(), 'yellow'),
		else:
			print colored("NO FIELDS!", 'red'),

		itsMethods = aClass.get_methods()
		if itsMethods:
			print colored("\nMethods:\n", 'yellow'), 
			for idx, aMethod in enumerate(itsMethods):
				print colored("===> METHOD#"+str(idx+1)+": "+aMethod.get_name()+" [IMPL BELOW:]", 'blue'),
				print colored(decompileMethod(aMethod, analysis), 'white')
				
		else:
			print colored("NO METHODS!", 'red'),
		print("\n"+11*"~=~=~=")
		
		
	

    print(rawdex.__class__)
    print(dex.__class__)
    print(analysis.__class__)

    # enumerate classes
    # for each class enumerate interfaces, fields, query super class
    # for each class enumerate and decompile methods
    # write the java file to disk



















